const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const path = require('path');
const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const cors = require('cors');

// Assign environment variables
const port = process.env.PORT || 4000;
// Assign environment variables
const mongoUri = process.env.MONGO_URI || "mongodb://localhost:27017/users";
// console.log(mongoUri)

/**
 * Setup services
 */

 // Setup routes
 const users = require("./routes/api/users");
 const emailController = require('./email/email.controller')

// Initiliaze an express server
const app = express();

// Initiliaze cors
app.use(cors({
  origin: 'http://localhost:3000',
  // origin: 'http://dykraf.com',
  credentials: true
}));

// Options to pass to mongodb to avoid deprecation warnings
const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
};

// Function to connect to the database
const conn = () => {
  mongoose.connect(
    mongoUri,
    options
  );
};
// Call it to connect
conn();

// Handle the database connection and retry as needed
const db = mongoose.connection;
db.on("error", err => {
  console.log("There was a problem connecting to mongo: ", err);
  console.log("Trying again");
  setTimeout(() => conn(), 4000);
});
db.once("open", () => console.log("Successfully connected to => mongo"));

// include before other routes
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Methods', '*');  // enables all the methods to take place
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

  return next();
});

// view engine setup for express server
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.get('/confirm/:id', emailController.confirmEmail);
app.use('/uploads', express.static('uploads'));
app.use("/api/users", users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Passport middleware
app.use(passport.initialize());

// Passport config
require("./config/passport")(passport);

app.listen(port, () => console.log(`Listening on port => ${port}`));
