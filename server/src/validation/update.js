const Validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateUpdateInput(data) {
  let errors = {};

  // Convert empty fields to an empty string so we can use validator functions
//   data.firstname = !isEmpty(data.firstname) ? data.firstname : "";
//   data.email = !isEmpty(data.email) ? data.email : "";
//   data.password = !isEmpty(data.password) ? data.password : "";
//   data.password2 = !isEmpty(data.password2) ? data.password2 : "";

  // Firstname checks
//   if (Validator.isEmpty(data.firstname)) {
//     errors.firstname = "Firstname field is required";
//   }

  // Email checks
//   if (Validator.isEmpty(data.email)) {
//     errors.email = "Email field is required";
//   } else if (!Validator.isEmail(data.email)) {
//     errors.email = "Email is invalid";
//   }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
