const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");

// Load input validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");
const validateUpdateInput = require("../../validation/update");

// Load User model
const User = require("../../models/User");

// Load email registration
// const emailController = require("../../email/email.controller");
const sendEmail = require('../../email/email.send')
const msgs = require('../../email/email.msgs')
const templates = require('../../email/email.templates')

// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", (req, res) => {

  // Form validation
  const { errors, isValid } = validateRegisterInput(req.body);

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      return res.status(400).json({ email: "Email already exists" });
    } else {
      const newUser = new User({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: req.body.password,
        birthdate: req.body.birthdate,
        phone: req.body.phone,
        country: req.body.country,
        hobby: req.body.hobby,
        occupation: req.body.occupation,
        avatar:'',
        confirmed: 0
      });

      // Hash password before saving in database
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then(newUser => sendEmail(newUser.email, templates.confirm(newUser._id)))
            .then(() => res.json({ msg: msgs.confirm }))
            .then(user => res.json({user}))
            .catch(err => console.log(err));
        });
      });
    }
  });
});

// @route POST api/users/login
// @desc Login user and return JWT token
// @access Public
router.post("/login", (req, res) => {
  // Form validation

  const { errors, isValid } = validateLoginInput(req.body);

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  // Find user by email
  User.findOne({ email }).then(user => {
    
    // Check if user exists
    if (!user) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }
    
    if (!user.confirmed) {
      return res.status(400).json({ emailnotfound: "Account not active, please check your email to activate!" });      
    }

    // Check password
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        // User matched
        // Create JWT Payload
        const payload = {
          id: user.id,
          email: user.email
        };
        // Sign token
        jwt.sign(
          payload,
          keys.secretOrKey,
          {
            expiresIn: 31556926 // 1 year in seconds
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        return res
          .status(400)
          .json({ passwordincorrect: "Password incorrect" });
      }
    });
  });
});


// @route POST api/users/update
// @desc Update user profile
// @access Public
router.post("/update", (req, res) => {
  // Form validation
  const { errors, isValid } = validateUpdateInput(req.body);

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  } else {

  const updateUser = {
        id: req.body.id,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        phone: req.body.phone,
        occupation: req.body.occupation,
        hobby: req.body.hobby,
        avatar: req.body.avatar
      };
      User.findByIdAndUpdate(req.body.id, updateUser)
      .then((user) => res.json(user))
      .catch(err => console.log(err))
  }
});

router.get('/:id', function(req, res, next) {
    User.findOne({_id: req.params.id}, function (err, user) { 
      if (err) throw err;
      const filter = {
        firstname: user.firstname,
        lastname:user.lastname,
        email:user.email,
        country:user.country,
        phone:user.phone,
        birthdate:user.birthdate,
        avatar:user.avatar,
        hobby:user.hobby,
        occupation:user.occupation,
        date:user.date
      }
      return res.json(filter)
    });
});

module.exports = router;
