module.exports = {
  mongoURI: "YOUR_MONGO_URI_HERE",
  secretOrKey: "secretOrKeyJWT",  
  CLIENT_ORIGIN: process.env.NODE_ENV === 'production' ? process.env.CLIENT_ORIGIN : 'http://localhost:3000'
};
