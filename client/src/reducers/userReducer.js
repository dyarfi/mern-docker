import { FETCH_USER, USER_LOADING } from "../actions/types";

const initialState = {
  data: {},
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_USER:
      return {
        ...state,
        data: action.payload
      };
    case USER_LOADING:
      return {
        ...state,
        loading: true
      };
    default:
      return state;
  }
}
