import React from 'react'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faSync } from '@fortawesome/free-solid-svg-icons'

export default props =>
  <div className={`fadeIn ${props.spinning} container`}>
    {/* <FontAwesomeIcon icon={faSync} size={props.size} /> */}
    <h5 className="font-weight-bold text-uppercase mx-auto m-5 p-5 fadeInOut">Result : <span className="text-info">{props.message}</span></h5>
  </div>