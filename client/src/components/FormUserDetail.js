import React, { Component } from 'react';
// import classnames from "classnames";



export class FormUserDetails extends Component {
    
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    };

  render() {
    const { values, disable, errors } = this.props;
    return (
      <div className="col-12 my-4 animate fadeIn">
        <React.Fragment>
          <h4>User Details</h4>
            <div className="form-row">
                <div className="col-6">
                    <div className="form-group">
                        <label>First Name</label>
                        <input
                            type="text"
                            placeholder="First Name"
                            className="form-control"
                            name="firstname"
                            onChange={this.props.handleChange('firstname')}                   
                            defaultValue={values.firstname}
                            error={errors.lastname}
                        />
                        <span className="text-danger">{errors.firstname}</span>
                    </div>
                </div>   
                <div className="col-6">
                    <div className="form-group">
                        <label>Last Name</label>            
                        <input
                            type="text"                            
                            placeholder="Last Name"
                            className="form-control"
                            name="lastname"                            
                            onChange={this.props.handleChange('lastname')}
                            defaultValue={values.lastname}
                            error={errors.lastname}
                        />
                        <span className="text-danger">{errors.lastname}</span>
                    </div>
                </div>
                <div className="col-12">
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            placeholder="Email"
                            className="form-control"
                            onChange={this.props.handleChange('email')}                   
                            defaultValue={values.email}
                            error={errors.email}
                        />
                        <span className="text-danger">{errors.email}</span>
                    </div>
                </div>
                <div className="col-sm-6">
                  <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input
                      onChange={this.props.handleChange('password')}
                      defaultValue={values.password}
                      error={errors.password}
                      id="password"
                      type="password"
                      name="password"                            
                      className="form-control"
                    //   className={classnames("form-control", {
                    //     invalid: errors.password
                    //   })}
                    />
                    <span className="text-danger">{errors.password}</span>
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="form-group">
                    <label htmlFor="password2">Confirm Password</label>
                    <input
                      onChange={this.props.handleChange('password2')}
                      defaultValue={values.password2}
                      className="form-control"
                      error={errors.password2}
                      id="password2"
                      type="password"
                      name="password2"
                    //   className={classnames("form-control", {
                    //     invalid: errors.password2
                    //   })}
                    />
                    <span className="text-danger">{errors.password2}</span>
                  </div>
              </div>                     
            </div>                           
            <button
                className="btn btn-primary float-right"
                onClick={this.continue}
                disabled={disable}
            >Continue</button>
        </React.Fragment>
      </div>
    );
  }
}

export default FormUserDetails;
