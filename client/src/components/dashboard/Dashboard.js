import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { updateUser, logoutUser, fetchUser } from "../../actions/authActions";
import FileBase from 'react-file-base64'; 
import DefaultImg from '../../assets/img-placeholder.jpg';

class Dashboard extends Component {

  constructor(props) {
    super(props)
    this.state = {
      id: '',
      firstname: '',
      lastname: '',
      phone: '',
      occupation: '',
      hobby: '',
      avatar: DefaultImg,
      errors: {},
    }
  }
  
  componentDidMount() {

    // Fetch user data
    this.props.fetchUser({id:this.props.auth.user.id})

  }

  componentWillReceiveProps(nextProps) {
    const user = nextProps.user.data;
    if (nextProps.errors) {
      this.setState({
        // Load errors
        errors: nextProps.errors,
        // Load user data
        firstname: user.firstname,
        lastname: user.lastname,
        phone: user.phone,
        occupation: user.occupation,
        hobby: user.hobby,
        avatar: (user.avatar) ? user.avatar : this.state.avatar,
      });
    }
  }

  // function to capture base64 format of an image
  getBaseFile(files) {
      
    this.setState({
      avatar:files.base64
    });      

  };

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    
    // Set variables
    const updatedUser = {
      id: this.props.auth.user.id,
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      phone: this.state.phone,
      occupation: this.state.occupation,
      hobby: this.state.hobby,
      avatar: (this.state.avatar) ? this.state.avatar : DefaultImg
    };
    
    // Set user data to be updated
    this.props.updateUser(updatedUser, this.props.history);
    // this.props.logoutUser();
  };

  render() {;
    
    // Set data to be rendered
    const { firstname, lastname, phone, birthdate, occupation, hobby, avatar  } = this.props.user.data;
    const values = { firstname, lastname, phone, birthdate, occupation, hobby, avatar };

    return (
      <div style={{ height: "75vh" }} className="container animate bounceInDown">
        <div className="row">
          <div className="col-sm-6 my-4 order-2">
            <h4><b>Hey there,</b> {values.firstname}</h4>
            <p className="flow-text grey-text text-darken-1">
              You are logged into a full-stack{" "}
              <span style={{ fontFamily: "monospace" }}>MERN</span> app 👏
            </p>
            <div className="pull-right">
              <button onClick={this.onLogoutClick} className="btn btn-outline-primary btn-md">Logout</button>
            </div>
          </div>
          <div className="col-sm-6 my-4 order-1">
            <h4>Update your profile</h4>
            <form encType="multipart/form-data" noValidate onSubmit={this.onSubmit} autoComplete="off">
              <input type="hidden" name="_id" defaultValue={values.id} />
              <div className="form-row">
                <div className="col-12 text-left">
                  <div className="form-group">
                    <label htmlFor="firstname">First Name</label>
                    <input name="firstname" id="firstname"
                      onChange={this.onChange}
                      defaultValue={values.firstname}
                      className="form-control" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="lastname">Last Name</label>
                    <input name="lastname" id="lastname"
                      onChange={this.onChange}
                      defaultValue={values.lastname}
                      className="form-control" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="phone">Phone</label>
                    <input name="phone" id="phone"
                      onChange={this.onChange}
                      defaultValue={values.phone}
                      className="form-control" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="occupation">Occupation</label>
                    <input name="occupation" id="occupation"
                      onChange={this.onChange}
                      defaultValue={values.occupation}
                      className="form-control" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="hobby">Hobby</label>
                    <input name="hobby" id="hobby"
                      onChange={this.onChange}
                      defaultValue={values.hobby}
                      className="form-control" />
                  </div>
                  <div className="form-group">
                    <div className="process">
                      <label htmlFor="customFile">Avatar</label>
                      <div className="custom-file">
                        <FileBase accept="image/*" type="file" id="customFile" className="custom-file-input" multiple={false} onDone={this.getBaseFile.bind(this)} />
                      </div>
                      <img src={this.state.avatar} alt="upload_image_multer" className="img-fluid" />
                    </div>
                  </div>
                  
                  <div className="form-group">
                    <button type="submit" className="btn btn-primary btn-block btn-lg">Submit</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  updateUser: PropTypes.func.isRequired,
  fetchUser: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  user: state.user,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { updateUser, fetchUser, logoutUser }
)(withRouter(Dashboard));
