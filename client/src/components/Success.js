import React, { Component } from 'react';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { logoutUser, fetchUser } from "../actions/authActions";
export class Success extends Component {

  // constructor(props) {
  //   super(props)        
  // }

  componentDidMount() {
    // If logged in and user navigates to Register page, should redirect them to dashboard
    // if (this.props.auth.isAuthenticated) {
      // this.props.history.push("/dashboard")
    // }
    
    // Fetch user data
    if (this.props.user.data.params !== undefined) {
      setTimeout(function() {
        this.props.fetchUser({id:this.props.match.params.id})
      },1000);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { id } = this.props.match.params;
    const { firstname, lastname, phone, birthdate, occupation, hobby, avatar  } = this.props.user.data
    const values = { firstname, lastname, phone, birthdate, occupation, hobby, avatar };
    return (
      <div>
        <React.Fragment>
          <div className="col-12">
            <h1>Success</h1>
            <h4>{(values && values.firstname !== undefined) ? values.firstname +', ' : ''}            
            {(id) ? 'Profile updated!' : 'Thank You For Submission'}
            </h4>
            {(id) ? '' : <p>You will get an email with further instructions</p> }
          </div>
        </React.Fragment>
      </div>
    );
  }
}

Success.propTypes = {
  fetchUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  user: state.user,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { fetchUser, logoutUser }
)(withRouter(Success));
