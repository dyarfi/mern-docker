import React, { Component } from 'react';
// import classnames from "classnames";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export class FormPersonalDetails extends Component {

    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }
    
    render() {
    const { values, handleChange, disable } = this.props;    
    return (
      <div className="col-12 my-4 animate fadeIn">
        <React.Fragment>
            <h4>Personal Details</h4>  
            <div className="form-row">
                <div className="col-4">
                    <div className="form-group">
                        <label htmlFor="country">Country</label>
                        <select id="country" name="country" className="custom-select" ref={this.textInput}   
                            onChange={this.props.handleChange('country')} 
                            >
                            {values.countries.map((country) => <option key={country.iso2} value={country.iso2} data-phone={country.dialCode}>{country.name}</option>)}
                        </select>
                    </div>
                </div>    
                <div className="col-4">
                    <div className="form-group">
                        <label htmlFor="phone">Phone</label>
                            <input
                            ref={this.textSelected}
                            type="phone"
                            onChange={this.props.handleChange('phone')}
                            value={values.phone}
                            placeholder=""
                            id="phone"
                            className="form-control"/>
                    </div>
                </div>
                <div className="col-4">
                    <div className="form-group">
                        <label className="d-block" htmlFor="birthdate">Birth Date</label>
                        <DatePicker
                            name="birthdate"
                            id="birthdate"
                            className="form-control"
                            selected={values.birthdate}
                            onChange={this.props.handleChange('birthdate')}
                        />
                    </div>
                </div>          
            </div>                      
            <div className="form-row">
                <div className="col-6">
                    <div className="form-group">
                        <label className="d-block" htmlFor="occupation">Occupation</label>
                        <input
                            name="occupation"
                            id="occupation"
                            placeholder="Occupation"
                            className="form-control"
                            onChange={handleChange('occupation')}
                            value={values.occupation}
                        />
                    </div>
                </div>
                <div className="col-6">
                    <div className="form-group">
                    <label htmlFor="hobby">Hobby</label>
                        <input
                            id="hobby"
                            placeholder="Hobby"
                            className="form-control"
                            name="hobby"
                            onChange={handleChange('hobby')}
                            value={values.hobby}
                        />
                    </div>
                </div>
            </div>            
            <button              
                className="btn btn-primary"
                onClick={this.back}
            >Back</button>
            <button
                className="btn btn-primary float-right"
                onClick={this.continue}
                disabled={disable}
            >Continue</button>
        </React.Fragment>
      </div>
    );
  }
}

export default FormPersonalDetails;
