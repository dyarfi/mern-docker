import React, { Component } from 'react';
import FormUserDetails from './FormUserDetail';
import FormPersonalDetails from './FormPersonalDetail';
import Confirm from './Confirm';
import Success from './Success';

export class UserForm extends Component {

  render() {
    const step = this.props.step;
    const nextStep = this.props.nextStep;
    const prevStep = this.props.prevStep;
    const disable = this.props.disable;
    const handleChange = this.props.handleChange;
    const errors = this.props.errors;
    const { firstname, lastname, email, password, password2, country, countries, birthdate, occupation, phone, hobby  } = this.props.values;
    const values = { firstname, lastname, email, password, password2, country, countries, birthdate, occupation, phone, hobby };
    switch (step) {
      case 1:
        return (
          <FormUserDetails
            nextStep={nextStep}
            prevStep={prevStep}
            handleChange={handleChange}
            values={values}
            disable={disable} 
            errors={errors}
          />
        );
      case 2:
        return (
          <FormPersonalDetails  
            nextStep={nextStep}
            prevStep={prevStep}
            handleChange={handleChange}
            values={values}
            errors={errors}
          />
        );
      case 3:
        return (
          <Confirm
            nextStep={nextStep}
            prevStep={prevStep}
            handleChange={handleChange}
            values={values}
            disable={disable}
            errors={errors}
          />
        );
      case 4:
        return <Success />;
      default:
        // do nothing
    }
  }
}

export default UserForm;
