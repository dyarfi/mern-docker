import React, { Component } from "react";
import { Link } from "react-router-dom";

class Navbar extends Component {
  render() {
    return (
      <div className="position-relative">
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="nav-wrapper white">
            <Link
              to="/"
              style={{
                fontFamily: "monospace"
              }}
              className="navbar-brand"
            >
              MERN
            </Link>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
