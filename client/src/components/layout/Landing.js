import React, { Component } from "react";
import { Link } from "react-router-dom";

class Landing extends Component {
  render() {
    return (
      <div style={{ height: "75vh" }} className="container animate bounceInDown">
        <div className="row">
          <div className="col-12 mx-auto my-5">
            <h4>
              <b>Build</b> a login/auth app with the{" "} MERN stack
            </h4>
            <p className="lead">
              Create a (minimal) full-stack app with user authentication via
              passport and JWTs
            </p>
            <br />
            <div className="row">
              <div className="col-6">
                <Link
                  to="/register"
                  style={{
                    width: "140px",
                    borderRadius: "3px",
                    letterSpacing: "1.5px"
                  }}
                  className="btn btn-large btn-primary"
                >
                  Register
                </Link>
              </div>
              <div className="col-6">
              <Link
                to="/login"
                style={{
                  width: "140px",
                  borderRadius: "3px",
                  letterSpacing: "1.5px"
                }}
                className="btn btn-large btn-primary"
              >
                Log In
              </Link>
            </div>
            </div>    
          </div>
        </div>
      </div>
    );
  }
}

export default Landing;
