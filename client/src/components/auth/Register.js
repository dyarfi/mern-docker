import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerUser } from "../../actions/authActions";
import { UserForm } from "../UserForm";
// import classnames from "classnames";
import countryTelData from "country-telephone-data";
// countryTelData.allCountries // has data as array of objects 
// countryTelData.iso2Lookup // has data as a map (object) indexed by iso2 name of the country

var date = new Date(); 
class Register extends Component {

  state = {
      firstname: "",
      lastname: "",
      email: "",
      birthdate: Date.parse(date),
      phone: "",
      country: "",
      hobby: "",
      password: "",
      password2: "",
      occupation: "",
      countries: countryTelData.allCountries,
      step: 1,
      disable:true,
      errors: {}
  }; 
       
  // Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1
    })
  }

  componentDidMount() {
    // If logged in and user navigates to Register page, should redirect them to dashboard
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard")
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  handleChange = input => e => {
    
    if(input === 'birthdate') {
      var picked = e;
      var dd = picked.getDate();
      var mm = picked.getMonth()+1; 
      var yyyy = picked.getFullYear();
      var _birthdate = `${mm.toString()}/${dd.toString()}/${yyyy.toString()}`;
      this.setState({
        birthdate: Date.parse(_birthdate)
      })
    } else {
      var _phone = ''
      if(input === 'country') {
        const index = e.target.selectedIndex
        const optionElement = e.target.childNodes[index]
        const option = optionElement.getAttribute('data-phone')
        _phone = '+' + option
        this.setState({
          phone: _phone
        })
      }
      var _disable = this.state.disable
      if(this.state.firstname.length > 1) {
        _disable = false      
      }
      this.setState({
        disable: _disable,
        [input]: e.target.value, 
      });
    }
  };

  onSubmit = e => {
    e.preventDefault();
    
    // Set user data
    const newUser = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2,
      phone: this.state.phone,
      birthdate: this.state.birthdate,
      country: this.state.country,
      hobby: this.state.hobby,
      occupation: this.state.occupation
    };

    this.props.registerUser(newUser, this.props.history);
    
  };

  render() {
    const { errors } = this.state;
    const step = this.state.step;
    const disable = this.state.disable;
    const { firstname, lastname, email, password, password2, country, phone, countries, birthdate, occupation, hobby } = this.state;
    const values = { firstname, lastname, email, password, password2, country, phone, countries, birthdate, occupation, hobby };
    // console.log(values);    

    return (
      <div className="container animate bounceInDown">
        <div style={{ marginTop: "4rem" }} className="row">
          <div className="col-6">
            <Link to="/" className="btn btn-primary mb-4">
              Back to
              home
            </Link>
            <h3>
              <b>Register</b> below
            </h3>
            <p className="grey-text text-darken-1">
              Already have an account? <Link to="/login">Log in</Link>
            </p>
            <div className="row">
              <form autoComplete="off" className="col-12" onSubmit={this.onSubmit}>
                <div className="row">
                  <UserForm handleChange={this.handleChange} prevStep={this.prevStep} nextStep={this.nextStep} 
                values={values} disable={disable} step={step} errors={errors}/>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { registerUser }
)(withRouter(Register));
