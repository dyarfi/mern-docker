import React, { Component } from 'react';

export class Confirm extends Component {
  
  onSubmit = e => {
    e.preventDefault();
    this.refs.register.onSubmit();
  };

  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const {
      values: { firstname, lastname, email, country, phone, birthdate, occupation, hobby }
    } = this.props;
    const errors = this.props.errors;    
    return (
      <div className="col-12 my-4 animate fadeIn">
        <React.Fragment>
          <h4>Confirm User Data</h4>
          <ul className="mx-4">
            <li>First Name : {firstname} <span className="text-danger">{errors.firstname}</span></li>
            <li>Last Name : {lastname} <span className="text-danger">{errors.lastname}</span></li>
            <li>Email : {email} <span className="text-danger">{errors.email}</span></li>
            <li>Country : {country} <span className="text-danger">{errors.country}</span></li>
            <li>Phone : {phone} <span className="text-danger">{errors.phone}</span></li>
            <li>Birthdate: {birthdate} <span className="text-danger">{errors.birthdate}</span></li>
            <li>Occupation: {occupation} <span className="text-danger">{errors.occupation}</span></li>
            <li>Hobby: {hobby} <span className="text-danger">{errors.hobby}</span></li>
            {(errors.password || errors.password2) ? <li>Password: <span className="text-danger">{errors.password||errors.password2}</span></li>
              : ''}
          </ul>
          <br />
          <button type="submit" className="btn btn-primary">Confirm &amp; Submit</button>
          <button onClick={this.back} className="btn btn-primary float-right">Back</button>
        </React.Fragment>
      </div>
    );
  }
}

export default Confirm;